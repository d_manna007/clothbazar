﻿using StudentExamReportCard.Models;
using StudentExamReportCard.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentExamReportCard.Controllers
{
    public class HomeController : Controller
    {
        StudentDBEntitie ObjStudentDBEntite;
        public HomeController()
        {
            ObjStudentDBEntite = new StudentDBEntitie();
        }
        // GET: Home
        public ActionResult Index()
        {
            StudentMasterViewModel ObjStudentMasterViewModel = new StudentMasterViewModel();
            ObjStudentMasterViewModel.ListOfExam = (from obj in ObjStudentDBEntite.Exams
                                                    select new SelectListItem()
                                                    {
                                                        Text = obj.ExamName,
                                                        Value = obj.ExamId.ToString()
                                                   }).ToList();
           
            ObjStudentMasterViewModel.ListOfSubject = (from obj in ObjStudentDBEntite.Subjects
                                                       select new SelectListItem()
                                                       {
                                                           Text = obj.SubjectName,
                                                           Value = obj.SubjectId.ToString()
                                                       }).ToList();
            return View(ObjStudentMasterViewModel);
        }
    }
}
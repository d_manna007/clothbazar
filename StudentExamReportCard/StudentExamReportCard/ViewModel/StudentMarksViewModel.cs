﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentExamReportCard.ViewModel
{
    public class StudentMarksViewModel
    {
        public int SubjectId { get; set; }
        public int TotalMarks { get; set; }
        public int MarksObtained { get; set; }
        public decimal Percentage { get; set; }
    }
}
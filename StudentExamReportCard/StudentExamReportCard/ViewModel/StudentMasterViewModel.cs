﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace StudentExamReportCard.ViewModel
{
    public class StudentMasterViewModel
    {
        
        [Display(Name="Student Name")]
        public string Name { get; set; }
        [Display(Name = "Class")]
        public string Class { get; set; }
        [Display(Name="Roll Number")]
        public int RollNumber { get; set; }
        [Display(Name ="Exam")]
        public int ExamId { get; set; }
        [Display(Name ="Subject")]
        public int SubjectId { get; set; }

        public IEnumerable<SelectListItem> ListOfExam { get; set; }

        public IEnumerable<SelectListItem> ListOfSubject { get; set; }
        [Display(Name="Total Marks")]
        public int TotalMarks { get; set; }
        [Display(Name ="Marks Obtained")]
        public int MarksObtained { get; set; }
        public decimal Percentage { get; set; }
    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(clothbazar.Startup))]
namespace clothbazar
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
